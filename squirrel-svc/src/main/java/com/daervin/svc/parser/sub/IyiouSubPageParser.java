package com.daervin.svc.parser.sub;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.dto.NewsDTO;
import com.daervin.svc.common.utils.DateTimeUtils;
import com.daervin.svc.common.utils.NumberUtil;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.Calendar;
import java.util.List;

import static com.daervin.svc.common.constants.SourceEnum.YI_OU;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class IyiouSubPageParser extends RootSubParser {
    public IyiouSubPageParser(String... listUrl) {
        super(listUrl);
    }

    @Override
    public MatchOther processPage(Page page) {
        Html html = page.getHtml();
        String title = html.xpath("//div[@id='post_content']//div[@id='post_title']/text()").get();
        String time = html.xpath("//div[@id='post_content']//div[@id='post_date']/text()").get();
        StringBuilder desc = new StringBuilder();
        List<Selectable> contentPList = html.xpath("//div[@id='post_content']//div[@id='post_description']/p").nodes();
        if (CollectionUtils.isEmpty(contentPList)) {
            return MatchOther.NO;
        }
        for (Selectable item : contentPList) {
            String pdesc = item.xpath("//p/text()").get();
            if (StringUtils.isEmpty(pdesc)) {
                continue;
            }
            desc.append(pdesc);
        }
        NewsDTO news = new NewsDTO();
        news.setTitle(title);
        news.setLinks(page.getRequest().getUrl());
        news.setCategory(YI_OU.category);
        news.setAnnouncer(YI_OU.announcer);
        if (!time.contains("分钟前") && !time.contains("小时前")) {
            return MatchOther.NO;
        }
        Calendar bdateTmp = Calendar.getInstance();
        if (time.contains("分钟前")) {
            time = time.replaceAll("分钟前", "");
            Integer minute = NumberUtil.safeParseNumber(time, Integer.class);
            if (minute == null) {
                return MatchOther.NO;
            }
            bdateTmp.add(Calendar.MINUTE, -minute);
        }
        if (time.contains("小时前")) {
            time = time.replaceAll("小时前", "");
            Integer hour = NumberUtil.safeParseNumber(time, Integer.class);
            if (hour == null) {
                return MatchOther.NO;
            }
            bdateTmp.add(Calendar.HOUR_OF_DAY, -hour);
        }
        if (StringUtils.isEmpty(desc) || StringUtils.isEmpty(title) || StringUtils.isEmpty(time)) {
            return MatchOther.NO;
        }
        news.setBelongDate(DateTimeUtils.longParse(bdateTmp.getTime()));
        news.setDesc(desc.toString());

        page.putField(Constants.PARSER_RESULT_ITEM, news);
        return MatchOther.YES;
    }
}
