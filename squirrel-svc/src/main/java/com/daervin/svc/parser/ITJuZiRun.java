package com.daervin.svc.parser;

import com.daervin.svc.common.constants.Category;
import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.dto.JuZiDTO;
import com.daervin.svc.common.dto.NewsDTO;
import com.daervin.svc.common.utils.DateTimeUtils;
import com.daervin.svc.common.utils.HttpClientUtil;
import com.daervin.svc.common.utils.JsonUtil;
import com.daervin.svc.common.utils.NumberUtil;
import com.daervin.svc.service.INewsService;
import okhttp3.Headers;
import okhttp3.Response;
import org.springframework.util.CollectionUtils;

import java.util.*;

import static com.daervin.svc.common.constants.SourceEnum.IT_JU_ZI;

/**
 * @author daervin
 * @date 2019/12/11
 */
public class ITJuZiRun implements Runnable {
    private INewsService service;

    public ITJuZiRun(INewsService service) {
        this.service = service;
    }

    @Override
    public void run() {
        try {
            //{"time":"2019-12-11"}
            Map<String, String> param = new HashMap<>();
            param.put("time", DateTimeUtils.shortParse(new Date()));
            Map<String, String> headers = new HashMap<>();
            headers.put("Accept", "application/json, text/plain, */*");
            headers.put("CURLOPT_FOLLOWLOCATION", "true");
            headers.put("Host", "www.itjuzi.com");
            headers.put("Origin", "https://www.itjuzi.com");
            headers.put("Referer", "https://www.itjuzi.com/bulletin");
            headers.put(" User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36 LBBROWSER");
            Response response = HttpClientUtil.post(Constants.URL_ITJuZi, JsonUtil.toString(param), Headers.of(headers));
            if (response == null || !response.isSuccessful() || response.body() == null) {
                return;
            }
            String data = response.body().string();
            JuZiDTO news = JsonUtil.toObject(data, JuZiDTO.class);

            if (news != null && !CollectionUtils.isEmpty(news.getData())) {
                List<NewsDTO> newsList = new ArrayList<>();
                news.getData().stream().forEach(item -> {
                    Long createTime = NumberUtil.safeParseNumber(item.getCreateTime(), Long.class);
                    if (createTime == null || createTime <= 0) {
                        return;
                    }
                    NewsDTO newsDTO = new NewsDTO();
                    newsDTO.setTitle(item.getTitle());
                    newsDTO.setAnnouncer(IT_JU_ZI.announcer);
                    newsDTO.setCategory(IT_JU_ZI.category);
                    if (item.getRound() != null && "投资事件".equalsIgnoreCase(item.getRound())) {
                        newsDTO.setCategory(Category.FINANCE.ordinal());
                    }
                    newsDTO.setDesc(item.getDes());
                    newsDTO.setBelongDate(DateTimeUtils.secondParse(createTime));
                    newsDTO.setLinks(item.getUrl());
                    newsList.add(newsDTO);
                });
//                System.out.println(JsonUtil.toString(newsList));
                if (service != null) {
                    service.commonAdd(newsList, "");
                }
            }
        } catch (Exception e) {
            System.err.println("ITJuZiRun" + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new ITJuZiRun(null).run();
    }
}
