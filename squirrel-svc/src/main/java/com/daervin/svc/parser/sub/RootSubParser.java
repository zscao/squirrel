package com.daervin.svc.parser.sub;

import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.handler.SubPageProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public abstract class RootSubParser implements SubPageProcessor {
    private List<String> listUrls = new ArrayList<>();

    public RootSubParser(String... listUrl) {
        if (listUrl != null && listUrl.length > 0) {
            for (String url : listUrl) {
                this.listUrls.add(url);
            }
        }
    }

    @Override
    public boolean match(Request page) {
        return !this.listUrls.contains(page.getUrl());
    }
}
