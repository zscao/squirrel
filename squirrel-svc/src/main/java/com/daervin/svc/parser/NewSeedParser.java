package com.daervin.svc.parser;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.dto.NewsDTO;
import com.daervin.svc.common.utils.DateTimeUtils;
import com.daervin.svc.common.utils.NumberUtil;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.daervin.svc.common.constants.SourceEnum.NEW_SEED;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class NewSeedParser extends RootParser {
    private final static Logger LOGGER = Logger.getLogger(NewSeedParser.class);

    public NewSeedParser(String url) {
        super(url);
    }

    @Override
    public void listProcess(Page page) {
        List<Selectable> newsViewList = page.getHtml().xpath("//ul[@id=\"quicklook-list\"]/").nodes();
        List<NewsDTO> newsList = new ArrayList<>();

        for (Selectable newsView : newsViewList) {
            Calendar bdateTmp = Calendar.getInstance();
            String targetStr = newsView.xpath("//h3[@class=\"title\"]/text()").get();
            String targetDesc = newsView.xpath("//div[@class=\"firstline-content\"]/p/text()").get();
            String publishTime = newsView.xpath("//div[@class=\"firstline-time\"]/time/text()").get();
            if (StringUtils.isEmpty(targetStr) || StringUtils.isEmpty(publishTime)) {
                continue;
            }
            if (publishTime.contains("分钟前")) {
                publishTime = publishTime.replaceAll("分钟前", "");
                Integer minute = NumberUtil.safeParseNumber(publishTime, Integer.class);
                if (minute == null) {
                    continue;
                }
                bdateTmp.add(Calendar.MINUTE, -minute);
            }
            if (publishTime.contains("小时前")) {
                publishTime = publishTime.replaceAll("小时前", "");
                Integer hour = NumberUtil.safeParseNumber(publishTime, Integer.class);
                if (hour == null) {
                    continue;
                }
                bdateTmp.add(Calendar.HOUR_OF_DAY, -hour);
            }
            if (publishTime.contains("天前")) {
                publishTime = publishTime.replaceAll("天前", "");
                Integer day = NumberUtil.safeParseNumber(publishTime, Integer.class);
                if (day == null) {
                    continue;
                }
                bdateTmp.add(Calendar.DAY_OF_MONTH, -day);
            }

            NewsDTO news = new NewsDTO();
            news.setCategory(NEW_SEED.category);
            news.setTitle(targetStr);
            news.setAnnouncer(NEW_SEED.announcer);
            news.setDesc(targetDesc == null ? "" : targetDesc);
            news.setBelongDate(DateTimeUtils.longParse(bdateTmp.getTime()));
            newsList.add(news);
        }
        page.putField(Constants.PARSER_RESULT, newsList);
    }
}
