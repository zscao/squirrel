package com.daervin.svc.parser;

import com.daervin.svc.common.constants.Constants;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.handler.CompositePageProcessor;
import us.codecraft.webmagic.handler.SubPageProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class RootParser extends CompositePageProcessor {
    private List<String> parentUrls = new ArrayList<>();

    public RootParser(String... parentUrls) {
        super(Site.me().setRetryTimes(1).setSleepTime(3000).setTimeOut(4000).setUserAgent(Constants.UA));
        if (parentUrls != null && parentUrls.length > 0) {
            for (String url : parentUrls) {
                this.parentUrls.add(url);
            }
        }
    }

    public RootParser(Site site) {
        super(site);
    }

    @Override
    public CompositePageProcessor setSite(Site site) {
        if (site == null) {
            site = Site.me().setRetryTimes(1).setSleepTime(3000).setTimeOut(4000).setUserAgent(Constants.UA);
        }
        return super.setSite(site);
    }

    @Override
    public CompositePageProcessor addSubPageProcessor(SubPageProcessor subPageProcessor) {
        return super.addSubPageProcessor(subPageProcessor);
    }

    @Override
    public CompositePageProcessor setSubPageProcessors(SubPageProcessor... subPageProcessors) {
        return super.setSubPageProcessors(subPageProcessors);
    }

    @Override
    public void process(Page page) {
        super.process(page);
        if (this.parentUrls.contains(page.getRequest().getUrl())) {
            this.listProcess(page);
        }
    }

    protected void listProcess(Page page) {

    }
}
