package com.daervin.svc.parser.hot;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.utils.NumberUtil;
import com.daervin.svc.dal.model.HotItem;
import com.daervin.svc.parser.RootParser;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.List;

import static com.daervin.svc.common.constants.HotSiteEnum.BAIDU;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class BaiduParser extends RootParser {
    private final static Logger LOGGER = Logger.getLogger(BaiduParser.class);

    public BaiduParser(String... parentUrls) {
        super(parentUrls);
    }

    @Override
    public Site getSite() {
        return super.getSite();
    }

    @Override
    public void listProcess(Page page) {
        Html html = page.getHtml();
        if (html == null) {
            return;
        }
        List<Selectable> newsViewList = html.xpath("//div[@class=\"grayborder\"]//table[@class=\"list-table\"]/tbody/").nodes();
        if (CollectionUtils.isEmpty(newsViewList)) {
            return;
        }
        List<HotItem> hotItemList = new ArrayList<>();
        int count = 0;
        for (Selectable item : newsViewList) {
            if (count++ == 0) {
                continue;
            }
            try {
                List<Selectable> node = item.xpath("tr[@class=\"item-tr\"]").nodes();
                if (node != null && node.size() > 0) {
                    continue;
                }
                String title = item.xpath("//td[@class=\"keyword\"]//a[@class=\"list-title\"]/text()").get();
                String links = item.xpath("//td[@class=\"keyword\"]//a[@class=\"list-title\"]").links().get();
                String rise = item.xpath("//td[@class=\"last\"]//span[@class=\"icon-rise\"]/text()").get();
                if (rise == null) {
                    rise = item.xpath("//td[@class=\"last\"]/span[@class=\"icon-fair\"]/text()").get();
                }
                if (rise == null) {
                    rise = item.xpath("//td[@class=\"last\"]/span[@class=\"icon-fall\"]/text()").get();
                }
                Long score = NumberUtil.safeParseNumber(rise, Long.class);
                hotItemList.add(new HotItem(BAIDU.ID, title, links, score));
            } catch (Exception e) {
                System.err.println("BaiduParser error: " + e.getMessage());
                LOGGER.error("BaiduParser error", e);
            }
        }
        if (CollectionUtils.isEmpty(hotItemList)) {
            return;
        }
        page.putField(Constants.PARSER_RESULT, hotItemList);
    }

}
