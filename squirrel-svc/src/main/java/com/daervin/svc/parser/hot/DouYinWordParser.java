package com.daervin.svc.parser.hot;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.dto.hot.WordBean;
import com.daervin.svc.common.dto.hot.WordList;
import com.daervin.svc.common.utils.JsonUtil;
import com.daervin.svc.dal.model.HotItem;
import com.daervin.svc.parser.RootParser;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Json;

import java.util.ArrayList;
import java.util.List;

import static com.daervin.svc.common.constants.HotSiteEnum.BAIDU;
import static com.daervin.svc.common.constants.HotSiteEnum.DOUYIN_WORD;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class DouYinWordParser extends RootParser {

    private final static Logger LOGGER = Logger.getLogger(DouYinWordParser.class);

    public DouYinWordParser(String url) {
        super(url);
    }

    @Override
    public void listProcess(Page page) {
        Json responseJson = page.getJson();
        if(responseJson == null){
            return;
        }
        WordBean response = responseJson.toObject(WordBean.class);
        if(response == null || CollectionUtils.isEmpty(response.getWordList())){
            return;
        }
        List<HotItem> hotItemList = new ArrayList<>();
        for (WordList word : response.getWordList()) {
            hotItemList.add(new HotItem(DOUYIN_WORD.ID, word.getWord(), "https://www.douyin.com/", word.getHotValue()));
        }
        if (CollectionUtils.isEmpty(hotItemList)) {
            return;
        }
        page.putField(Constants.PARSER_RESULT, hotItemList);
    }
}
