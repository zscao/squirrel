package com.daervin.svc.parser.hot;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.utils.NumberUtil;
import com.daervin.svc.dal.model.HotItem;
import com.daervin.svc.parser.RootParser;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.List;

import static com.daervin.svc.common.constants.HotSiteEnum.WEIBO;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class WeiboParser extends RootParser {
    private final static Logger LOGGER = Logger.getLogger(WeiboParser.class);

    public WeiboParser(String... parentUrls) {
        super(parentUrls);
    }

    @Override
    public Site getSite() {
        return super.getSite();
    }

    @Override
    public void listProcess(Page page) {
        Html html = page.getHtml();
        if (html == null) {
            return;
        }
        List<Selectable> newsViewList = html.xpath("//div[@id=\"pl_top_realtimehot\"]//table//tbody//tr").nodes();
        if (CollectionUtils.isEmpty(newsViewList)) {
            return;
        }
        List<HotItem> hotItemList = new ArrayList<>();
        for (Selectable item : newsViewList) {
            try {
                String title = item.xpath("//td[@class=\"td-02\"]//a/text()").get();
                String links = item.xpath("//td[@class=\"td-02\"]//a").links().get();
                String scoreStr = item.xpath("//td[@class=\"td-02\"]//span//text()").get();
                if(StringUtils.isEmpty(title) || StringUtils.isEmpty(links) || StringUtils.isEmpty(scoreStr)){
                    continue;
                }
                Long score = NumberUtil.safeParseNumber(scoreStr, Long.class);
                hotItemList.add(new HotItem(WEIBO.ID, title, links, score));
            } catch (Exception e) {
                System.err.println("WeiboParser error: " + e.getMessage());
                LOGGER.error("WeiboParser error", e);
            }
        }
        if (CollectionUtils.isEmpty(hotItemList)) {
            return;
        }
        page.putField(Constants.PARSER_RESULT, hotItemList);
    }

}
