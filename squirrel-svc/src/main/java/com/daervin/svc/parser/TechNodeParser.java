package com.daervin.svc.parser;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class TechNodeParser extends RootParser {

    private final static Logger LOGGER = Logger.getLogger(TechNodeParser.class);

    public TechNodeParser(String url) {
        super(url);
    }

    @Override
    public void listProcess(Page page) {
        Html html = page.getHtml();
        if (html == null) {
            return;
        }

        String xpathStr = "//*[@class=\"row-container\"]";
        List<Selectable> newsViewList = html.xpath(xpathStr).nodes();
        for (Selectable newsView : newsViewList) {
            String rowTitle = newsView.xpath("//*[@class=\"heading-text\"]//h2//a//text()").get();
            if (StringUtils.isEmpty(rowTitle) || !"最新".equalsIgnoreCase(rowTitle)) {
                continue;
            }
            List<Selectable> entryTextList = newsView.xpath("//*[@class=\"t-entry-text\"]").nodes();
            for (Selectable entryTextView : entryTextList) {
                try {
                    String title = entryTextView.xpath("//*[@class=\"t-entry-title\"]//a//text()").get();
                    String links = entryTextView.xpath("//*[@class=\"t-entry-title\"]//a").links().get();
                    String dateTime = entryTextView.xpath("//*[@class=\"t-entry-date\"]//text()").get();
                    if (StringUtils.isEmpty(title) || StringUtils.isEmpty(links) || StringUtils.isEmpty(dateTime)) {
                        continue;
                    }
                    if (title.contains("早8点") || title.contains("早 8 点档")) {
                        continue;
                    }

                    Request targetRequest = new Request();
                    targetRequest.setUrl(links);
                    page.addTargetRequest(targetRequest);
                } catch (Exception e) {
                    System.err.println("TechNodeParser error: " + e.getMessage());
                    LOGGER.error("TechNodeParser error", e);
                }
            }
        }

    }
}
