package com.daervin.svc.parser.sub;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.dto.NewsDTO;
import com.daervin.svc.common.utils.DateTimeUtils;
import com.daervin.svc.common.utils.NumberUtil;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Html;

import java.util.Calendar;

import static com.daervin.svc.common.constants.SourceEnum.TMT_POST;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class TmtpostSubPageParser extends RootSubParser {

    public TmtpostSubPageParser(String... listUrl) {
        super(listUrl);
    }

    @Override
    public MatchOther processPage(Page page) {
        Html html = page.getHtml();
        if (html == null) {
            return MatchOther.NO;
        }
        String desc = html.xpath("//div[@class='word_part']/div[@class='main']/pre/text()").get();
        String title = html.xpath("//div[@class='word_part']/h2/text()").get();
        String time = html.xpath("//div[@class='word_part']/p[@class='time']/text()").get();
        time = time == null ? "" : time.trim();
        if (StringUtils.isEmpty(desc) || StringUtils.isEmpty(title) || StringUtils.isEmpty(time)) {
            return MatchOther.NO;
        }
        Calendar bdateTmp = Calendar.getInstance();
        if (time.contains("分钟前")) {
            time = time.replaceAll("分钟前", "");
            Integer minute = NumberUtil.safeParseNumber(time, Integer.class);
            if (minute == null) {
                return MatchOther.NO;
            }
            bdateTmp.add(Calendar.MINUTE, -minute);
        }
        if (time.contains("小时前")) {
            time = time.replaceAll("小时前", "");
            Integer hour = NumberUtil.safeParseNumber(time, Integer.class);
            if (hour == null) {
                return MatchOther.NO;
            }
            bdateTmp.add(Calendar.HOUR_OF_DAY, -hour);
        }
        NewsDTO news = new NewsDTO();
        news.setTitle(title);
        news.setLinks(page.getRequest().getUrl());
        news.setCategory(TMT_POST.category);
        news.setDesc(desc);
        news.setAnnouncer(TMT_POST.announcer);
        news.setBelongDate(DateTimeUtils.longParse(bdateTmp.getTime()));

        page.putField(Constants.PARSER_RESULT_ITEM, news);
        return MatchOther.YES;
    }
}
