package com.daervin.svc.parser;

import com.daervin.svc.common.utils.DateTimeUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class HexunParser extends RootParser {
    private final static Logger LOGGER = Logger.getLogger(HexunParser.class);

    public HexunParser(String... parentUrls) {
        super(parentUrls);
    }

    @Override
    public Site getSite() {
        return super.getSite();
    }

    @Override
    public void listProcess(Page page) {
        Html html = page.getHtml();
        if (html == null) {
            return;
        }
        List<Selectable> newsViewList = html.xpath("//ul[@class=\"ul14\"]/").nodes();
        if (CollectionUtils.isEmpty(newsViewList)) {
            return;
        }
        String today = DateTimeUtils.longParse4(new Date());
        for (Selectable item : newsViewList) {
            try {
                String span = item.xpath("//span/text()").get();
                if (span == null || !span.contains(today)) {
                    continue;
                }
                String title = item.xpath("//a/text()").get();
                String links = item.xpath("//a").links().get();

                Request targetRequest = new Request();
                targetRequest.setUrl(links);
                page.addTargetRequest(targetRequest);
            } catch (Exception e) {
                System.err.println("HexunParser error: " + e.getMessage());
                LOGGER.error("HexunParser error", e);
            }
        }
    }
    
}
