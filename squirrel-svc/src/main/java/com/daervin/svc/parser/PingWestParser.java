package com.daervin.svc.parser;

import com.daervin.svc.common.dto.PingWestResponseDTO;
import com.daervin.svc.common.dto.PingWestResponseDataDTO;
import com.daervin.svc.common.utils.JsonUtil;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Json;
import us.codecraft.webmagic.selector.Selectable;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class PingWestParser extends RootParser {
    private final static Logger LOGGER = Logger.getLogger(PingWestParser.class);

    public PingWestParser(String url) {
        super(url);
    }

    @Override
    public void listProcess(Page page) {
        Json responseJson = page.getJson();
        PingWestResponseDTO response = responseJson.toObject(PingWestResponseDTO.class);
        if (response == null || StringUtils.isEmpty(response.getData())) {
            return;
        }
        PingWestResponseDataDTO responseData = JsonUtil.toObject(response.getData(), PingWestResponseDataDTO.class);
        if (responseData == null || StringUtils.isEmpty(responseData.getList())) {
            return;
        }
        Html html = Html.create(responseData.getList());
        if (html == null) {
            return;
        }
        List<Selectable> newsViewList = html.xpath("/html/body/").nodes();
        for (Selectable newsView : newsViewList) {
            try {
                String publishTime = newsView.xpath("//section[@class=\"time\"]//span//text()").get();
                if (StringUtils.isEmpty(publishTime)) {
                    continue;
                }
                String title = newsView.xpath("//section[@class=\"news-info\"]//p[@class=\"title\"]/a/text()").get();
                String links = newsView.xpath("//section[@class=\"news-info\"]//p[@class=\"title\"]/a").links().get();
                if (StringUtils.isEmpty(title) || StringUtils.isEmpty(links)) {
                    continue;
                }

                Request targetRequest = new Request();
                targetRequest.setUrl("https:" + links);
                page.addTargetRequest(targetRequest);

            } catch (Exception e) {
                System.err.println("PingWestParser error: " + e.getMessage());
                LOGGER.error("PingWestParser error", e);
            }

        }
    }
}
