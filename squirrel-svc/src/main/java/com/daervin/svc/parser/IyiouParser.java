package com.daervin.svc.parser;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.selector.Selectable;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class IyiouParser extends RootParser {
    private final static Logger LOGGER = Logger.getLogger(IyiouParser.class);

    public IyiouParser(String url) {
        super(url);
    }

    @Override
    public Site getSite() {
        return super.getSite();
    }

    @Override
    public void listProcess(Page page) {
        String xpathStr = "//div[@class='box_zxlistcontent']//ul/";
        List<Selectable> newsViewList = page.getHtml().xpath(xpathStr).nodes();
        for (Selectable item : newsViewList) {
            try {
                String title = item.xpath("//a/span[@class='fl']/text()").get();
                String time = item.xpath("//a/span[@class='fr']/text()").get();
                String links = item.xpath("//a").links().get();
                if (StringUtils.isEmpty(links) || StringUtils.isEmpty(title) || StringUtils.isEmpty(time)) {
                    continue;
                }
                Request targetRequest = new Request();
                targetRequest.setUrl(links);
                page.addTargetRequest(targetRequest);
            } catch (Exception e) {
                System.err.println("IyiouParser error: " + e.getMessage());
                LOGGER.error("IyiouParser error", e);
            }
        }
    }
}
