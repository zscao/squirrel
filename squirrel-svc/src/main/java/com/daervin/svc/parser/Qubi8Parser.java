package com.daervin.svc.parser;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.dto.NewsDTO;
import com.daervin.svc.common.utils.DateTimeUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.daervin.svc.common.constants.SourceEnum.QU_BI8;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class Qubi8Parser extends RootParser {

    private final static Logger LOGGER = Logger.getLogger(Qubi8Parser.class);

    public Qubi8Parser(String... url) {
        super(url);
    }

    @Override
    public void listProcess(Page page) {
        Html html = page.getHtml();
        if (html == null) {
            return;
        }
        String date = html.xpath("//*[@class=\"kx-date\"]/text()").get();
        List<Selectable> newsViewList = html.xpath("//*[@class=\"kx-item\"]").nodes();
        if (StringUtils.isEmpty(date) || !date.contains("今天") || CollectionUtils.isEmpty(newsViewList)) {
            return;
        }
        List<NewsDTO> newsList = new ArrayList<>();
//        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        String dateShortStr = DateTimeUtils.shortParse(new Date());
        for (Selectable item : newsViewList) {
            String title = item.xpath("//*[@class=\"kx-content\"]/h2/a/text()").get();
            String desc = item.xpath("//*[@class=\"kx-content\"]/p/text()").get();
            String dateStr = item.xpath("//*[@class=\"kx-time\"]/text()").get();
            if (StringUtils.isEmpty(title) || StringUtils.isEmpty(desc) || StringUtils.isEmpty(dateStr)) {
                continue;
            }
            NewsDTO dto = new NewsDTO();
            dto.setTitle(title);
            dto.setDesc(desc);
            dto.setBelongDate(dateShortStr + " " + dateStr);
            dto.setAnnouncer(QU_BI8.announcer);
            dto.setCategory(QU_BI8.category);
            dto.setLinks("");
            newsList.add(dto);
        }
        page.putField(Constants.PARSER_RESULT, newsList);
    }
}
