package com.daervin.svc.common.dto;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class PingWestResponseDTO {
    private Integer status;
    private String message;
    private String data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
