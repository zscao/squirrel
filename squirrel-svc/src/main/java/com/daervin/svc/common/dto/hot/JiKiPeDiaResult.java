package com.daervin.svc.common.dto.hot;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class JiKiPeDiaResult {
    @JsonProperty("updated_at")
    private Date updatedAt;
    private List<JiKiPeDiaData> data;

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<JiKiPeDiaData> getData() {
        return data;
    }

    public void setData(List<JiKiPeDiaData> data) {
        this.data = data;
    }
}
