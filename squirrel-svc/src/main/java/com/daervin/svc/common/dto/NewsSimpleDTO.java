package com.daervin.svc.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author daervin
 * @date 2019/12/6
 */
public class NewsSimpleDTO {
    private Long id;
    // 新闻标题
    private String title;
    //发布结构
    private String announcer;

    private Long pid;

    @JsonIgnore
    private double rate;

    public NewsSimpleDTO() {
    }

    public NewsSimpleDTO(String title) {
        this.title = title;
    }

    public NewsSimpleDTO(String title, String announcer) {
        this.title = title;
        this.announcer = announcer;
    }

    public NewsSimpleDTO(Long id, String title, String announcer, Long pid) {
        this.id = id;
        this.title = title;
        this.announcer = announcer;
        this.pid = pid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnnouncer() {
        return announcer;
    }

    public void setAnnouncer(String announcer) {
        this.announcer = announcer;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
