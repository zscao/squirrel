package com.daervin.svc.common.dto.hot;

public class MetricsArea {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
