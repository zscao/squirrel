package com.daervin.svc.common.dto.hot;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZhiHuHotData {
    @JsonProperty("detail_text")
    private String detailText;
    private ZhiHuTarget target;

    public String getDetailText() {
        return detailText;
    }

    public void setDetailText(String detailText) {
        this.detailText = detailText;
    }

    public ZhiHuTarget getTarget() {
        return target;
    }

    public void setTarget(ZhiHuTarget target) {
        this.target = target;
    }
}
