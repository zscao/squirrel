package com.daervin.svc.common.constants;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class Constants {
    private Constants() {
    }

    public static final String PARSER_RESULT = "result";
    public static final String PARSER_RESULT_ITEM = "result_item";

    public static final String IMAGE_24_DOMAIN = "https://www.daervin.com/news/images/24-solar-terms";
    public static final String IMAGE_DOMAIN = "https://www.daervin.com/news/images/download/";
    public static final String IMAGE_LOCALPATH = "E:\\htdocs\\news\\images\\download\\";
    //                public static final String IMAGE_LOCALPATH = "F:\\download\\";
    public static final String IMAGE_PIXABAY = "https://pixabay.com/api/?key=11023152-8461993c218a8cfbd1cf61704&image_type=photo&category=computer&lang=zh&min_width=355&min_height=150&orientation=horizontal&page=";

    public static final int DELAY_TIME_MINUTE_1 = 1000 * 60 * 1;
    public static final int DELAY_TIME_MINUTE_5 = 1000 * 60 * 5;
    public static final int DELAY_TIME_MINUTE_10 = 1000 * 60 * 10;
    public static final int DELAY_TIME_MINUTE_20 = DELAY_TIME_MINUTE_10 * 2;
    public static final int DELAY_TIME_MINUTE_25 = 1000 * 60 * 25;
    public static final int DELAY_TIME_MINUTE_30 = DELAY_TIME_MINUTE_10 * 3;
    public static final int DELAY_TIME_MINUTE_35 = 1000 * 60 * 35;
    public static final int DELAY_TIME_MINUTE_40 = 1000 * 60 * 40;

    public static final String URL_JIEMIAN_KJ = "https://www.jiemian.com/lists/84.html";
    public static final String URL_JIEMIAN_RZ = "https://www.jiemian.com/lists/142.html";
    public static final String URL_CHINA_VENTURE = "https://www.chinaventure.com.cn/flash/list.html";
    public static final String URL_PEDAILY = "https://www.pedaily.cn/i-internet";
    public static final String URL_PINGWEST_P1 = "https://www.pingwest.com/api/state/list?page=1";
    public static final String URL_NEWSEED = "https://www.newseed.cn/quicklook/p1";
    public static final String URL_IYIOU_P1 = "https://www.iyiou.com/breaking/p1.html";
    public static final String URL_TMTPOST_P1 = "http://www.tmtpost.com/nictation/1";
    public static final String URL_36KR = "https://36kr.com/api/newsflash?b_id=0&per_page=100&_=";
    public static final String URL_HUXIU_IMG = "https://www-api.huxiu.com/v1/article/list?pagesize=22";
    public static final String URL_HISTORY = "http://www.todayonhistory.com/index.php?c=index&a=json_event&page=%s&pagesize=1&month=%s&day=%s";
    public static final String URL_TECH_NODE_P1 = "https://cn.technode.com";
    public static final String URL_TECH_NODE_P2 = "https://cn.technode.com/?upage=2";
    public static final String URL_QUBI8_P1 = "https://www.qubi8.com/wp-admin/admin-ajax.php?action=wpcom_load_kuaixun&page=1";
    public static final String URL_QUBI8_P2 = "https://www.qubi8.com/wp-admin/admin-ajax.php?action=wpcom_load_kuaixun&page=1";
    public static final String URL_QUBI8_P3 = "https://www.qubi8.com/wp-admin/admin-ajax.php?action=wpcom_load_kuaixun&page=1";
    public static final String URL_WALIAN = "https://www.walian.cn/live";
    public static final String URL_BTC = "https://www.8btc.com/flash";
    public static final String URL_ITJuZi = "https://www.itjuzi.com/api/newsletter";
    public static final String URL_HE_XUN = "http://tech.hexun.com/highlightsa";

    public static final String URL_WEIXIN_SOGOU = "http://weixin.sogou.com/weixin?type=1&s_from=input&query=%s&ie=utf8&_sug_=y&_sug_type_=";

    public static final String UA = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36 LBBROWSER";

    public static final int CACHE_KEY_EXPIRE = 60 * 10 * 10; // 秒
    public static final String CACHE_KEY_IMG = "dv_img";
    public static final String CACHE_KEY_IMG_JIEQI = "dv_img_jieqi";
    public static final String CACHE_KEY_NEWS_FINANCE = "dv_news_2";
    public static final String CACHE_KEY_NEWS_IT_P1 = "dv_news_1_p1";
}
