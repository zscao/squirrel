package com.daervin.svc.common.dto.hot;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class WordBean {
    @JsonProperty("status_code")
    private int statusCode;
    @JsonProperty("active_time")
    private String activeTime;
    @JsonProperty("word_list")
    private List<WordList> wordList;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(String activeTime) {
        this.activeTime = activeTime;
    }

    public List<WordList> getWordList() {
        return wordList;
    }

    public void setWordList(List<WordList> wordList) {
        this.wordList = wordList;
    }
}
