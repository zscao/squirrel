package com.daervin.svc.common.constants;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public enum ImageType {
    NORMAL,
    PIXABAY,
    HU_XIU
}
