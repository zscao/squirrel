package com.daervin.svc.common.dto;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class ThreeKRDataDTO {
    private List<ThreeKRItemDTO> items;

    public List<ThreeKRItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ThreeKRItemDTO> items) {
        this.items = items;
    }
}
