package com.daervin.svc.common.dto.hot;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class AwemeList {
    @JsonProperty("aweme_info")
    private AwemeInfo awemeInfo;
    @JsonProperty("hot_value")
    private Long hotValue;
    private Long label;

    public AwemeInfo getAwemeInfo() {
        return awemeInfo;
    }

    public void setAwemeInfo(AwemeInfo awemeInfo) {
        this.awemeInfo = awemeInfo;
    }

    public Long getHotValue() {
        return hotValue;
    }

    public void setHotValue(Long hotValue) {
        this.hotValue = hotValue;
    }

    public Long getLabel() {
        return label;
    }

    public void setLabel(Long label) {
        this.label = label;
    }
}
