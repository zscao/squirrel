package com.daervin.svc.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期时间工具类。
 *
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public final class DateTimeUtils {

    private static final Logger logger = LoggerFactory.getLogger(DateTimeUtils.class);

    private static final String[] WEEK_ARRAY = {"未知", "周一", "周二", "周三", "周四", "周五", "周六", "周日"};
    /**
     * 时间格式（yyyy-MM-dd HH:mm）
     */
    public static final String LONG_TIME_STR = "yyyy-MM-dd HH:mm";
    public static final String LONG_TIME_STR2 = "yyyy-MM-dd-HH-mm-ss";
    public static final String LONG_TIME_STR3 = "yyyy-MM-dd HH:mm:ss";

    public static final String SHORT_TIME_STR = "yyyy-MM-dd";

    /**
     * SimpleDateFormat（yyyy-MM-dd HH:mm）
     */
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(LONG_TIME_STR);
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT2 = new SimpleDateFormat(LONG_TIME_STR2);
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT3 = new SimpleDateFormat(LONG_TIME_STR3);
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT4 = new SimpleDateFormat("MM/dd");

    public static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat(SHORT_TIME_STR);

    // 让工具类彻底不可以实例化
    private DateTimeUtils() {
        throw new Error("工具类不可以实例化！");
    }

    /**
     * 将字符串形式的时间（yyyy-MM-dd HH:mm）转换为毫秒
     *
     * @param strDateTime 字符串形式的时间（yyyy-MM-dd HH:mm）
     * @return 毫秒
     * @version 1.0.0
     * @since 1.0.0
     */
    public static long strDateTimeToMillis(String strDateTime) {
        long millis = -1;

        if (StringUtils.isNotBlank(strDateTime)) {
            try {
                millis = SIMPLE_DATE_FORMAT.parse(strDateTime).getTime();
            } catch (Exception e) {
                logger.error("strDateTimeToMillis error", e);
            }
        }

        return millis;
    }

    public static Date longParseDate(String strDateTime) {
        try {
            if(strDateTime.length() == 19){
                return SIMPLE_DATE_FORMAT3.parse(strDateTime);
            } else {
                return SIMPLE_DATE_FORMAT.parse(strDateTime);
            }
        } catch (ParseException e) {
            logger.error("longParseDate error", e);
        }
        return null;
    }

    public static Date shortParseDate(String strDateTime) {
        try {
            return SHORT_DATE_FORMAT.parse(strDateTime);
        } catch (ParseException e) {
            logger.error("shortParseDate error", e);
        }
        return null;
    }

    public static String longParse(Date date) {
        try {
            return SIMPLE_DATE_FORMAT.format(date);
        } catch (Exception e) {
            logger.error("longParse error", e);
        }
        return null;
    }

    public static String longParse2(Date date) {
        try {
            return SIMPLE_DATE_FORMAT2.format(date);
        } catch (Exception e) {
            logger.error("longParse error", e);
        }
        return null;
    }

    public static String longParse4(Date date) {
        try {
            return SIMPLE_DATE_FORMAT4.format(date);
        } catch (Exception e) {
            logger.error("longParse error", e);
        }
        return null;
    }

    public static String shortParse(Date date) {
        try {
            return SHORT_DATE_FORMAT.format(date);
        } catch (Exception e) {
            logger.error("shortParse error", e);
        }
        return null;
    }

    public static String longParse(Long mills) {
        try {
            Calendar cl = Calendar.getInstance();
            cl.setTimeInMillis(mills);
            return SIMPLE_DATE_FORMAT.format(cl.getTime());
        } catch (Exception e) {
            logger.error("longParse error", e);
        }
        return null;
    }

    public static String secondParse(Long mills) {
        try {
            Calendar cl = Calendar.getInstance();
            cl.setTimeInMillis(mills * 1000);
            return SIMPLE_DATE_FORMAT.format(cl.getTime());
        } catch (Exception e) {
            logger.error("longParse error", e);
        }
        return null;
    }

    public static Date getBeforeDefault() {
        Calendar cl = Calendar.getInstance();
        cl.add(Calendar.DAY_OF_MONTH, -1);
        cl.set(Calendar.HOUR_OF_DAY, 18);
        cl.set(Calendar.MINUTE, 0);
        cl.set(Calendar.SECOND, 0);
        cl.set(Calendar.MILLISECOND, 0);
        return cl.getTime();
    }

    public static Date getStartTime(Calendar cl) {
        cl.set(Calendar.HOUR_OF_DAY, 0);
        cl.set(Calendar.MINUTE, 0);
        cl.set(Calendar.SECOND, 0);
        cl.set(Calendar.MILLISECOND, 0);
        return cl.getTime();
    }

    /**
     * 根据日期获取周几（汉字）
     *
     * @param date
     * @return
     */
    public static String getWeekStrByDate(Calendar date) {
        return WEEK_ARRAY[getWeekNumberByDate(date)];
    }

    /**
     * 根据日期获取周几（数字）
     *
     * @param date
     * @return
     */
    public static int getWeekNumberByDate(Calendar date) {
        if (date == null) {
            return 0;
        }
        int dayOfWeek = date.get(Calendar.DAY_OF_WEEK);
        // 1=星期日 7=星期六，其他类推
        switch (dayOfWeek) {
            case 1:
                return 7;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 4;
            case 6:
                return 5;
            case 7:
                return 6;
            default:
                return 0;
        }
    }
}
