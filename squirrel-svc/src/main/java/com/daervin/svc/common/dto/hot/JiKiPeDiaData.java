package com.daervin.svc.common.dto.hot;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class JiKiPeDiaData {
    private String phrase;
    private String description;

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
