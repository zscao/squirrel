package com.daervin.svc.common.dto.hot;

import java.util.List;

public class ZhiHuHotResult {

    private List<ZhiHuHotData> data;

    public List<ZhiHuHotData> getData() {
        return data;
    }

    public void setData(List<ZhiHuHotData> data) {
        this.data = data;
    }
}
