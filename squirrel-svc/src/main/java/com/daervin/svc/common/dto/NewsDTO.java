package com.daervin.svc.common.dto;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class NewsDTO {
    // 新闻标题
    private String title;
    //描述
    private String desc;
    //哪一天的新闻
    private String belongDate;
    //发布结构
    private String announcer;
    //初步分类（0 其它新闻、1科技新闻、2、融资收购）
    private int category;
    //链接
    private String links;

    public NewsDTO() {
    }

    public NewsDTO(String title, String announcer) {
        this.title = title;
        this.announcer = announcer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(String belongDate) {
        this.belongDate = belongDate;
    }

    public String getAnnouncer() {
        return announcer;
    }

    public void setAnnouncer(String announcer) {
        this.announcer = announcer;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getLinks() {
        return links;
    }

    public void setLinks(String links) {
        this.links = links;
    }
}
