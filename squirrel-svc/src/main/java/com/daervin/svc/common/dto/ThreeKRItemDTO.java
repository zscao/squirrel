package com.daervin.svc.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class ThreeKRItemDTO {
    private Long id;
    private String title;
    private String description;
    @JsonProperty("news_url")
    private String newsUrl;
    @JsonProperty("published_at")
    private String publishedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNewsUrl() {
        return newsUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }
}
