package com.daervin.svc.common.dto.img;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class DataItem {
    private String pic_path;
    private Integer is_audio;
    private String origin_pic_path;
    private CountInfo count_info;
    private Integer viewnum;
    private String big_pic_path;

    public String getPic_path() {
        return pic_path;
    }

    public void setPic_path(String pic_path) {
        this.pic_path = pic_path;
    }

    public Integer getIs_audio() {
        return is_audio;
    }

    public void setIs_audio(Integer is_audio) {
        this.is_audio = is_audio;
    }

    public String getOrigin_pic_path() {
        return origin_pic_path;
    }

    public void setOrigin_pic_path(String origin_pic_path) {
        this.origin_pic_path = origin_pic_path;
    }

    public CountInfo getCount_info() {
        return count_info;
    }

    public void setCount_info(CountInfo count_info) {
        this.count_info = count_info;
    }

    public String getBig_pic_path() {
        return big_pic_path;
    }

    public void setBig_pic_path(String big_pic_path) {
        this.big_pic_path = big_pic_path;
    }

    public Integer getViewnum() {
        return viewnum;
    }

    public void setViewnum(Integer viewnum) {
        this.viewnum = viewnum;
    }
}
