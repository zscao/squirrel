package com.daervin.svc.common.dto;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class JuZiDTO {
    private Integer code;
    private List<JuZiItemDTO> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<JuZiItemDTO> getData() {
        return data;
    }

    public void setData(List<JuZiItemDTO> data) {
        this.data = data;
    }
}

