package com.daervin.svc.common.dto.hot;

public class ZhiHuLink {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
