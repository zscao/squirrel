package com.daervin.svc.common.dto;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class PingWestResponseDataDTO {
    private String listDate;
    private Integer count;
    private String list;

    public String getListDate() {
        return listDate;
    }

    public void setListDate(String listDate) {
        this.listDate = listDate;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }
}
