package com.daervin.svc.common.dto.hot;

public class ZhiHuTitleArea {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
