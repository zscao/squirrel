package com.daervin.svc.common.dto;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class ThreeKRDTO {
    private Integer code;
    private ThreeKRDataDTO data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public ThreeKRDataDTO getData() {
        return data;
    }

    public void setData(ThreeKRDataDTO data) {
        this.data = data;
    }
}

