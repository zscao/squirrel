package com.daervin.svc.common.constants;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public enum Category {
    NA("未分类"),
    TECH("科技及周边"),
    FINANCE("融资收购"),
    BLOCKCHAIN("区块链");

    Category(String desc) {
        this.desc = desc;
    }

    public String desc;
}
