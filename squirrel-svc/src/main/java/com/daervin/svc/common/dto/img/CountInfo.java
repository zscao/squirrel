package com.daervin.svc.common.dto.img;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class CountInfo {
    private Integer viewnum;

    public Integer getViewnum() {
        return viewnum;
    }

    public void setViewnum(Integer viewnum) {
        this.viewnum = viewnum;
    }
}
