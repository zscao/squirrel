package com.daervin.svc.common.dto.img;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class ImgDTO {
    private boolean success;
    private String message;
    private int code;
    private DataResult data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataResult getData() {
        return data;
    }

    public void setData(DataResult data) {
        this.data = data;
    }
}
