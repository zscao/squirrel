package com.daervin.svc.common.dto.hot;

import java.util.List;

public class BilibiliHotData {
    private String note;
    private List<BilibiliHotList> list;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<BilibiliHotList> getList() {
        return list;
    }

    public void setList(List<BilibiliHotList> list) {
        this.list = list;
    }
}
