package com.daervin.svc.common.constants;

/**
 * 源文件
 *
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public enum SourceEnum {
    CHINA_VENTURE(Category.TECH, "投中网"),
    HE_XUN(Category.TECH, "和讯科技"),
    IT_JU_ZI(Category.TECH, "IT桔子"),
    YI_OU(Category.TECH, "亿欧网"),
    JIE_MIAN(Category.TECH, "界面网"),
    NEW_SEED(Category.FINANCE, "新芽newseed"),
    PE_DAILY(Category.FINANCE, "投资界网"),
    PING_WEST(Category.TECH, "品玩"),
    QU_BI8(Category.BLOCKCHAIN, "趣币网"),
    TECH_NODE(Category.TECH, "动点科技"),
    KR_36(Category.TECH, "36氪"),
    TMT_POST(Category.TECH, "钛媒体"),
    ;

    public int category;

    public String announcer;

    SourceEnum(Category category, String announcer) {
        this.category = category.ordinal();
        this.announcer = announcer;
    }
}
