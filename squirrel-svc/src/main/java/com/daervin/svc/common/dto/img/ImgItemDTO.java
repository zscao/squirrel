package com.daervin.svc.common.dto.img;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class ImgItemDTO {
    private String webformatURL;
    private Integer likes;

    public String getWebformatURL() {
        return webformatURL;
    }

    public void setWebformatURL(String webformatURL) {
        this.webformatURL = webformatURL;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }
}
