package com.daervin.svc.common.dto.hot;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZhiHuTarget {
    private Long id;
    private String title;
    private String url;

    @JsonProperty("title_area")
    private ZhiHuTitleArea titleArea;

    @JsonProperty("metrics_area")
    private MetricsArea metricsArea;

    private ZhiHuLink link;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ZhiHuTitleArea getTitleArea() {
        return titleArea;
    }

    public void setTitleArea(ZhiHuTitleArea titleArea) {
        this.titleArea = titleArea;
    }

    public MetricsArea getMetricsArea() {
        return metricsArea;
    }

    public void setMetricsArea(MetricsArea metricsArea) {
        this.metricsArea = metricsArea;
    }

    public ZhiHuLink getLink() {
        return link;
    }

    public void setLink(ZhiHuLink link) {
        this.link = link;
    }
}
