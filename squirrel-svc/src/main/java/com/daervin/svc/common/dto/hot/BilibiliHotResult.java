package com.daervin.svc.common.dto.hot;

import java.util.List;

public class BilibiliHotResult {
    private List<BilibiliHotData> data;

    public List<BilibiliHotData> getData() {
        return data;
    }

    public void setData(List<BilibiliHotData> data) {
        this.data = data;
    }
}
