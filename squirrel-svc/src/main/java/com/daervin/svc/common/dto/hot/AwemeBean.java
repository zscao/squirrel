package com.daervin.svc.common.dto.hot;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class AwemeBean {
    @JsonProperty("aweme_list")
    private List<AwemeList> awemeList;
    @JsonProperty("status_code")
    private int statusCode;
    @JsonProperty("active_time")
    private String activeTime;

    public List<AwemeList> getAwemeList() {
        return awemeList;
    }

    public void setAwemeList(List<AwemeList> awemeList) {
        this.awemeList = awemeList;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(String activeTime) {
        this.activeTime = activeTime;
    }
}
