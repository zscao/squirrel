package com.daervin.svc.common.dto.img;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class PixabayImgDTO {
    private Integer totalHits;

    private List<ImgItemDTO> hits;

    public Integer getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(Integer totalHits) {
        this.totalHits = totalHits;
    }

    public List<ImgItemDTO> getHits() {
        return hits;
    }

    public void setHits(List<ImgItemDTO> hits) {
        this.hits = hits;
    }
}
