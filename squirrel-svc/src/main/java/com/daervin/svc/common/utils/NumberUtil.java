package com.daervin.svc.common.utils;

import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class NumberUtil {

    private static final char[] DIGITS_64 = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
            'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
            '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
            'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
            'Z', '+', '/'};

    private static final char[] DIGITS_62 = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
            'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E',
            'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
            'V', 'W', 'X', 'Y', 'Z'};

    private NumberUtil() {
    }

    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-", "").toLowerCase();
    }

    /**
     * 将字符串转换成知道类型的数字包装类
     *
     * @param text
     * @param targetClass
     * @param <T>
     * @return
     */
    public static <T extends Number> T safeParseNumber(String text, Class<T> targetClass) {
        T result = null;
        Map<String, String> attrs = new HashMap<>();
        attrs.put("text", text == null ? "null" : text);
        try {
            result = NumberUtils.parseNumber(text, targetClass);
        } catch (Exception e) {

        }
        return result;
    }

    /**
     * 把10进制的数字转换成64进制
     *
     * @param number
     * @return
     */
    public static String parse64Encode(long number) {
        char[] buf = new char[64];
        int charPos = 64;
        int radix = 1 << 6;
        long mask = radix - 1L;
        do {
            buf[--charPos] = DIGITS_64[(int) (number & mask)];
            number >>>= 6;
        } while (number != 0);
        return new String(buf, charPos, 64 - charPos);
    }

    /**
     * 把64进制的字符串转换成10进制
     *
     * @param decompStr
     * @return
     */
    public static long parse64Decode(String decompStr) {
        long result = 0;
        if (StringUtils.isEmpty(decompStr)) {
            return result;
        }
        for (int i = decompStr.length() - 1; i >= 0; i--) {
            for (int j = 0; j < DIGITS_64.length; j++) {
                if (decompStr.charAt(i) == DIGITS_64[j]) {
                    result += ((long) j) << 6 * (decompStr.length() - 1 - i);
                }
            }
        }
        return result;
    }

    /**
     * 无符号十进制long转62进制字符串
     *
     * @param number
     * @return
     */
    public static String parse62Encode(long number) {
        int digitIndex = 0;
        long longPositive = Math.abs(number);
        int radix = DIGITS_62.length;
        char[] outDigits = new char[63];
        for (; digitIndex <= radix; digitIndex++) {
            if (longPositive == 0) {
                break;
            }
            outDigits[outDigits.length - digitIndex - 1] = DIGITS_62[(int) longPositive % radix];
            longPositive /= radix;
        }
        return new String(outDigits, outDigits.length - digitIndex, digitIndex);
    }

    /**
     * 62进制字符串转十进制long
     *
     * @param value
     * @return
     */
    public static long parse62Decode(String value) {
        long result = 0L;
        int fromBase = DIGITS_62.length;
        if (StringUtils.isEmpty(value)) {
            return 0L;
        }
        String[] valueArr = value.split("");
        String digits = new String(DIGITS_62);
        for (int i = 0; i < value.length(); i++) {
            if (!digits.contains(valueArr[i])) {
                return -1L;
            }
            int index = 0;
            for (int xx = 0; xx < fromBase; xx++) {
                if (DIGITS_62[xx] == value.charAt(value.length() - i - 1)) {
                    index = xx;
                    break;
                }
            }
            result += (long) Math.pow(fromBase, i) * index;
        }
        return result;
    }

}
