package com.daervin.svc.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class ShortUrlDTO {
    @JsonProperty("Code")
    private int code;
    @JsonProperty("ShortUrl")
    private String shortUrl;
    @JsonProperty("LongUrl")
    private String longUrl;
    @JsonProperty("ErrMsg")
    private String errMsg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    @Override
    public String toString() {
        return "ShortUrlDTO{" +
                "code=" + code +
                ", shortUrl='" + shortUrl + '\'' +
                ", longUrl='" + longUrl + '\'' +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}
