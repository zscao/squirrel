package com.daervin.svc.common.dto.hot;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class WordList {
    private String word;
    @JsonProperty("search_word")
    private String searchWord;
    @JsonProperty("hot_value")
    private Long hotValue;
    private Integer label;
    @JsonProperty("challenge_id")
    private String challengeId;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }

    public Long getHotValue() {
        return hotValue;
    }

    public void setHotValue(Long hotValue) {
        this.hotValue = hotValue;
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }
}
