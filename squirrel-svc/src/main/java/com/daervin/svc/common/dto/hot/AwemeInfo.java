package com.daervin.svc.common.dto.hot;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class AwemeInfo {
    @JsonProperty("share_url")
    private String shareUrl;
    private String desc;

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
