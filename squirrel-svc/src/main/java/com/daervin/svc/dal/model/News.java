package com.daervin.svc.dal.model;

import java.util.Date;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class News {
    private Long id;
    // 新闻标题
    private String title;
    //描述
    private String description;
    //发布时间
    private Date belongDate;
    //发布结构
    private String announcer;
    //初步分类（0 其它新闻、1科技新闻、2、融资收购）
    private int category;
    //链接
    private String links;
    //状态 0无效，1全站有效，2PC有效
    private Integer status;
    //父级联id
    private Long pid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public String getAnnouncer() {
        return announcer;
    }

    public void setAnnouncer(String announcer) {
        this.announcer = announcer;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getLinks() {
        return links;
    }

    public void setLinks(String links) {
        this.links = links;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
}
