package com.daervin.svc.dal;

import com.daervin.svc.dal.model.Hot;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public interface IHotDao {
    Integer insert(Hot hot);

    Integer update(Hot hot);

    Hot selectByHotId(Integer hotId);
}
