package com.daervin.svc.dal.model;

import java.util.Date;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class SpiderInfo {
    private Long id;

    private String name;

    private Date lastTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }
}
