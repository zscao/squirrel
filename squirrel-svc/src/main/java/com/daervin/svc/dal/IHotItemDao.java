package com.daervin.svc.dal;

import com.daervin.svc.dal.model.HotItem;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public interface IHotItemDao {
    Integer insert(List<HotItem> hotItemList);

    Integer deleteByHotId(Integer hotId);
}
