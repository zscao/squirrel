package com.daervin.svc.dal.model;

import java.util.Date;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class HotItem {
    private Long id;
    private Integer hotId;
    private String title;
    private String link;
    private Long score;
    private Date lastTime;

    public HotItem() {
    }

    public HotItem(Integer hotId, String title, String link, Long score) {
        this.hotId = hotId;
        this.title = title;
        this.link = link;
        this.score = score == null ? 0 : score;
        this.lastTime = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getHotId() {
        return hotId;
    }

    public void setHotId(Integer hotId) {
        this.hotId = hotId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }
}
