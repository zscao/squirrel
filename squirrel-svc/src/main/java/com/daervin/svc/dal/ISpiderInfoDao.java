package com.daervin.svc.dal;

import com.daervin.svc.dal.model.SpiderInfo;

import java.util.Date;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ISpiderInfoDao {
    Integer insert(SpiderInfo info);

    Integer update(SpiderInfo info);

    SpiderInfo getByName(String name);

    Date getLastTime();
}
