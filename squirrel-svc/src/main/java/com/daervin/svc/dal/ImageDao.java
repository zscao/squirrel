package com.daervin.svc.dal;

import com.daervin.svc.dal.model.Image;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ImageDao {
    Integer insertBatch(List<Image> list);

    List<String> queryByType(Integer type);

    Integer deleteByType(Integer type);
}