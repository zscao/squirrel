package com.daervin.svc.dal.model;

import com.daervin.svc.common.constants.HotSiteEnum;

import java.util.Date;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class Hot {
    private Long id;
    private Integer hotId;
    private Integer siteId;
    private String siteName;
    private String topic;
    private Integer total;
    private Date lastTime;

    public Hot() {
    }

    public Hot(HotSiteEnum hotSiteEnum) {
        this.setHotId(hotSiteEnum.ID);
        this.setSiteId(hotSiteEnum.SITE_ID);
        this.setSiteName(hotSiteEnum.SITE_NAME);
        this.setTopic(hotSiteEnum.TOPIC);
        this.setTotal(null);
        this.setLastTime(new Date());
    }

    public Hot(HotSiteEnum hotSiteEnum, int total) {
        this.setHotId(hotSiteEnum.ID);
        this.setSiteId(hotSiteEnum.SITE_ID);
        this.setSiteName(hotSiteEnum.SITE_NAME);
        this.setTopic(hotSiteEnum.TOPIC);
        this.setTotal(total);
        this.setLastTime(new Date());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getHotId() {
        return hotId;
    }

    public void setHotId(Integer hotId) {
        this.hotId = hotId;
    }

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }
}
