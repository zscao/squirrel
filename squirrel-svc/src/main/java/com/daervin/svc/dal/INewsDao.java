package com.daervin.svc.dal;

import com.daervin.svc.dal.model.News;

import java.util.Date;
import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public interface INewsDao {

    Integer insert(List<News> news);

    List<News> selectsAll(Date startTime);

    List<String> selectsAllTitle(Date startTime);

    Integer count(String title);

    Integer deleteAll(Date endTime);
}
