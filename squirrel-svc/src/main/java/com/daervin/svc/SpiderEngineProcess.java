package com.daervin.svc;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.utils.HttpClientDownloader;
import com.daervin.svc.parser.*;
import com.daervin.svc.parser.sub.*;
import com.daervin.svc.pipeline.NewsDaoPipeline;
import com.daervin.svc.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
public class SpiderEngineProcess {
    @Autowired
    private INewsService service;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    public static Runnable threadIyiou(INewsService service) {
        String url = Constants.URL_IYIOU_P1;
        return Spider.create(new IyiouParser(url).addSubPageProcessor(new IyiouSubPageParser(url))).addUrl(url).addPipeline(new NewsDaoPipeline(service, url));
    }

    public static Runnable threadJieMian(INewsService service) {
        String[] urls = {Constants.URL_JIEMIAN_KJ, Constants.URL_JIEMIAN_RZ};
        return Spider.create(new JieMianParser(urls).addSubPageProcessor(new JiemianSubPageParser(urls))).addUrl(urls).addPipeline(new NewsDaoPipeline(service, urls));
    }

    public static Runnable threadTmtpost(INewsService service) {
        String url = Constants.URL_TMTPOST_P1;
        return Spider.create(new TmtpostParser(url).addSubPageProcessor(new TmtpostSubPageParser(url))).addUrl(url).addPipeline(new NewsDaoPipeline(service, url));
    }

    public static Runnable threadTechNode(INewsService service) {
        String url = Constants.URL_TECH_NODE_P1;
        return Spider.create(new TechNodeParser(url).addSubPageProcessor(new TechNodeSubPageParser(url))).addUrl(url).addPipeline(new NewsDaoPipeline(service, url));
    }

    public static Runnable threadChinaVenture(INewsService service) {
        String url = Constants.URL_CHINA_VENTURE;
        return Spider.create(new ChinaVentureParser(url)).addUrl(url).addPipeline(new NewsDaoPipeline(service, url));
    }

    public static Runnable threadNewSeed(INewsService service) {
        String url = Constants.URL_NEWSEED;
        return Spider.create(new NewSeedParser(url)).addUrl(url).addPipeline(new NewsDaoPipeline(service, url));
    }

    public static Runnable threadPedaily(INewsService service) {
        String url = Constants.URL_PEDAILY;
        return Spider.create(new PedailyParser(url)).addUrl(url).addPipeline(new NewsDaoPipeline(service, url));
    }

    public static Runnable threadPingWest(INewsService service) {
        String url = Constants.URL_PINGWEST_P1;
        return Spider.create(new PingWestParser(url).addSubPageProcessor(new PingWestSubPageParser(url))).addUrl(url).addPipeline(new NewsDaoPipeline(service, url));
    }

    public static Runnable threadThreeKR(INewsService service) {
        String url = Constants.URL_36KR + System.currentTimeMillis();
        return Spider.create(new ThreeKRParser(url)).addUrl(url).addPipeline(new NewsDaoPipeline(service, url));
    }

    public static Runnable threadITJuZi(INewsService service) {
        return new ITJuZiRun(service);
    }

    public static Runnable threadQubi8(INewsService service) {
        String[] urls = {Constants.URL_QUBI8_P1};
        return Spider.create(new Qubi8Parser(urls)).setDownloader(new HttpClientDownloader()).addUrl(urls).addPipeline(new NewsDaoPipeline(service, urls));
    }

    public static Runnable threadHexun(INewsService service) {
        String url = Constants.URL_HE_XUN;
        return Spider.create(new HexunParser(url).addSubPageProcessor(new HexunSubPageParser(url))).addUrl(url).addPipeline(new NewsDaoPipeline(service, url));
    }

//    public static Runnable threadHeaderImg(ImageService service) {
//        String url = Constants.URL_HUXIU_IMG;
//        return Spider.create(new HeaderImgParser(url)).addUrl(url).addPipeline(new ImgDaoPipeline(service));
//    }

//    public static Runnable thread24JieQi(ImageService service) {
//        String url = "http://api.jisuapi.com/jieqi/query?appkey=fd589f9aefc63480&year=";
//        return Spider.create(new JieQiParser(url)).addUrl(url).addPipeline(new JieQiPipeline(service));
//    }

    public static Thread takeThread(INewsService service) {
        Thread takeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    service.takeProcess();
                }
            }
        });
        takeThread.setName("TAKE");
        return takeThread;
    }

    public void submitNewsJob() {
        service.delBeforeData();

        taskExecutor.submit(SpiderEngineProcess.threadIyiou(service));
        taskExecutor.submit(SpiderEngineProcess.threadJieMian(service));
        taskExecutor.submit(SpiderEngineProcess.threadTmtpost(service));
        taskExecutor.submit(SpiderEngineProcess.threadTechNode(service));
        taskExecutor.submit(SpiderEngineProcess.threadChinaVenture(service));
        taskExecutor.submit(SpiderEngineProcess.threadNewSeed(service));
        taskExecutor.submit(SpiderEngineProcess.threadPedaily(service));
        taskExecutor.submit(SpiderEngineProcess.threadPingWest(service));
        taskExecutor.submit(SpiderEngineProcess.threadThreeKR(service));
        taskExecutor.submit(SpiderEngineProcess.threadQubi8(service));
        taskExecutor.submit(SpiderEngineProcess.threadITJuZi(service));
        taskExecutor.submit(SpiderEngineProcess.threadHexun(service));
    }

    public void takeThread() {
        taskExecutor.submit(SpiderEngineProcess.takeThread(service));
    }
}
