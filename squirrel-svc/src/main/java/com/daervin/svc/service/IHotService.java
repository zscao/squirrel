package com.daervin.svc.service;

import com.daervin.svc.dal.model.Hot;
import com.daervin.svc.dal.model.HotItem;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public interface IHotService {

    void addItems(List<HotItem> hotItemList, Hot hot);

    void initHot();
}
