package com.daervin.svc.service;

import com.daervin.svc.common.dto.NewsDTO;
import com.daervin.svc.common.dto.NewsSimpleDTO;
import com.daervin.svc.dal.model.News;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public interface INewsService {

    void delBeforeData();

    int addBatch(List<News> news);

    void commonAdd(List<NewsDTO> newsList, String name);

    void takeProcess();

    void addOrUpdateSpiderInfo(String name);

    List<NewsSimpleDTO> getAllExistData();
}
