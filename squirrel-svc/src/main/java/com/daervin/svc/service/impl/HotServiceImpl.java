package com.daervin.svc.service.impl;

import com.daervin.svc.common.constants.HotSiteEnum;
import com.daervin.svc.common.utils.JsonUtil;
import com.daervin.svc.dal.IHotDao;
import com.daervin.svc.dal.IHotItemDao;
import com.daervin.svc.dal.model.Hot;
import com.daervin.svc.dal.model.HotItem;
import com.daervin.svc.service.IHotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
public class HotServiceImpl implements IHotService {
    @Autowired
    private IHotDao hotDao;
    @Autowired
    private IHotItemDao hotItemDao;

    @Override
    public void addItems(List<HotItem> hotItemList, Hot hot) {
        System.out.println(String.format("----hot---%s--: %s", hotItemList.size(), JsonUtil.toString(hot)));
        hotItemDao.deleteByHotId(hot.getHotId());
        hotItemDao.insert(hotItemList);
        hotDao.update(hot);
    }

    @Override
    public void initHot() {
        for (HotSiteEnum hotSiteEnum : HotSiteEnum.values()) {
            Hot hot = hotDao.selectByHotId(hotSiteEnum.ID);
            if (hot == null) {
                hot = new Hot(hotSiteEnum);
                hotDao.insert(hot);
            }
        }
    }
}
