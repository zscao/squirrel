package com.daervin.svc.service;

import com.daervin.svc.common.constants.ImageType;
import com.daervin.svc.dal.model.Image;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ImageService {

    List<String> queryByType(ImageType type);

    void synchronizePixabay();

    void bannerImg(List<Image> list);
}

