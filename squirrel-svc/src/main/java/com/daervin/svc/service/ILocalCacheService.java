package com.daervin.svc.service;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.concurrent.Callable;

/**
 * @author daervin
 * @date 2019/12/6
 */
public interface ILocalCacheService {
    void clearAll();

    void put(String key, String value);

    <V> V getValue(String key, Class<V> vClass, Callable<String> valueLoader);

    <V> V getValue(String key, TypeReference<V> jsonTypeReference, Callable<String> valueLoader);
}
