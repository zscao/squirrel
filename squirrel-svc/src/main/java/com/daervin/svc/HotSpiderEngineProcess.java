package com.daervin.svc;

import com.daervin.svc.parser.hot.*;
import com.daervin.svc.pipeline.HotDaoPipeline;
import com.daervin.svc.service.IHotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;

import static com.daervin.svc.common.constants.HotSiteEnum.*;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
public class HotSpiderEngineProcess {
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    private IHotService service;

    public Runnable threadBaidu() {
        String url = BAIDU.TOPIC_URL;
        return Spider.create(new BaiduParser(url)).addUrl(url).addPipeline(new HotDaoPipeline(service));
    }

    public Runnable threadZhiHuDaily() {
        String url = ZHIHU_DAILY.ORG_URL;
        return Spider.create(new ZhiHuDailyParser(url)).addUrl(url).addPipeline(new HotDaoPipeline(service));
    }

    public Runnable threadWeibo() {
        String url = WEIBO.ORG_URL;
        return Spider.create(new WeiboParser(url)).addUrl(url).addPipeline(new HotDaoPipeline(service));
    }

    public Runnable threadDouYinWord() {
        String url = DOUYIN_WORD.TOPIC_URL;
        return Spider.create(new DouYinWordParser(url)).addUrl(url).addPipeline(new HotDaoPipeline(service));
    }

    public Runnable threadDouYinVideo() {
        String url = DOUYIN_VIDEO.TOPIC_URL;
        return Spider.create(new DouYinVideoParser(url)).addUrl(url).addPipeline(new HotDaoPipeline(service));
    }

    public Runnable threadDouJiKiPeDia() {
        return new JiKiPeDiaRun(service);
    }

    public Runnable threadZhiHu() {
        String[] urls = {ZHIHU_HOT.TOPIC_URL, ZHIHU_SPORT.TOPIC_URL, ZHIHU_CAR.TOPIC_URL};
        return Spider.create(new ZhiHuHotParser(urls)).addUrl(urls).addPipeline(new HotDaoPipeline(service));
    }

    public Runnable threadBiliBili() {
        String url = BILI_BILI.TOPIC_URL;
        return Spider.create(new BiliBiliParser(url)).addUrl(url).addPipeline(new HotDaoPipeline(service));
    }

    public void submit() {
        taskExecutor.submit(threadBaidu());
        taskExecutor.submit(threadZhiHuDaily());
        taskExecutor.submit(threadWeibo());
        taskExecutor.submit(threadWeibo());
        taskExecutor.submit(threadDouYinWord());
        taskExecutor.submit(threadDouYinVideo());
        taskExecutor.submit(threadDouJiKiPeDia());
        taskExecutor.submit(threadZhiHu());
        taskExecutor.submit(threadBiliBili());
    }
}
