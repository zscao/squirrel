package com.daervin.svc.pipeline;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.dto.NewsDTO;
import com.daervin.svc.common.utils.JsonUtil;
import com.daervin.svc.service.INewsService;
import org.springframework.util.CollectionUtils;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class NewsDaoPipeline implements Pipeline {
    private List<String> listUrls = new ArrayList<>();

    private INewsService service;

    public NewsDaoPipeline(INewsService service) {
        this.service = service;
    }

    public NewsDaoPipeline(INewsService service, String... listUrl) {
        if (listUrl != null && listUrl.length > 0) {
            for (String url : listUrl) {
                this.listUrls.add(url);
            }
        }
        this.service = service;
    }

    @Override
    public void process(ResultItems resultItems, Task task) {
        if (!this.listUrls.contains(resultItems.getRequest().getUrl())) {
            subPageProcess(resultItems, task);
            return;
        }
        pageProcess(resultItems, task);
    }

    protected void pageProcess(ResultItems resultItems, Task task) {
        List<NewsDTO> newsList = resultItems.get(Constants.PARSER_RESULT);
        if (CollectionUtils.isEmpty(newsList)) {
            return;
        }
        if (service == null) {
            System.err.println("***********测试环境***********");
            System.err.println(JsonUtil.toString(newsList));
            return;
        }
        service.commonAdd(newsList, "news-process");
    }

    protected void subPageProcess(ResultItems resultItems, Task task) {
        NewsDTO news = resultItems.get(Constants.PARSER_RESULT_ITEM);
        List<NewsDTO> newsList = new ArrayList<>();
        if (news == null) {
            return;
        }
        newsList.add(news);
        if (service == null) {
            System.err.println("***********测试环境***********");
            System.err.println(JsonUtil.toString(news));
            return;
        }
        service.commonAdd(newsList, "news-process");
    }
}
