package com.daervin.svc.pipeline;

import com.daervin.svc.common.constants.Constants;
import com.daervin.svc.common.constants.HotSiteEnum;
import com.daervin.svc.common.utils.JsonUtil;
import com.daervin.svc.dal.model.Hot;
import com.daervin.svc.dal.model.HotItem;
import com.daervin.svc.service.IHotService;
import org.springframework.util.CollectionUtils;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public class HotDaoPipeline implements Pipeline {
    private IHotService service;

    public HotDaoPipeline(IHotService service) {
        this.service = service;
    }

    @Override
    public void process(ResultItems resultItems, Task task) {
        List<HotItem> hotItemList = resultItems.get(Constants.PARSER_RESULT);
        if (CollectionUtils.isEmpty(hotItemList)) {
            return;
        }
        if (service == null) {
            System.err.println("***********测试环境***********");
            System.err.println(JsonUtil.toString(hotItemList));
            return;
        }
        HotItem hotItem = hotItemList.get(0);
        HotSiteEnum hotSiteEnum = HotSiteEnum.valOf(hotItem.getHotId());
        service.addItems(hotItemList, new Hot(hotSiteEnum, hotItemList.size()));
    }
}
