package com.daervin.svc.helper;

import com.daervin.svc.common.dto.NewsDTO;
import com.daervin.svc.common.dto.NewsSimpleDTO;
import com.daervin.svc.common.utils.DateTimeUtils;
import com.daervin.svc.common.utils.JsonUtil;
import com.daervin.svc.dal.model.News;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
public final class FilterHelper {
    private FilterHelper() {
    }

    public static boolean title(String title) {
        if (StringUtils.isEmpty(title)) {
            return false;
        }
        if (title.length() < 5) {
            return false;
        }
        return true;
    }

    public static boolean financingTitle(String title) {
        if (!title(title)) {
            return false;
        }
        Pattern pattern = Pattern.compile("(融资|领投|投资)");
        Matcher matcher = pattern.matcher(title);
        if (!matcher.find()) {
//            System.out.println("************************： " + title);
            return false;
        }
        return true;
    }

    public static boolean publishDate(String time) {
        if (StringUtils.isEmpty(time)) {
            return false;
        }

        return true;
    }

    public static boolean news(NewsDTO dto, News news) {
        if (dto == null) {
            return false;
        }
        if (!title(dto.getTitle()) || !publishDate(dto.getBelongDate())) {
            return false;
        }
        Pattern pattern = Pattern.compile("(融资|领投|投资)");
        Matcher matcher = pattern.matcher(dto.getTitle());
        if (matcher.find() && news.getCategory() != 2) {
            news.setCategory(2);
        }
        Pattern pattern2 = Pattern.compile("(区块链|数字货币)");
        Matcher matcher2 = pattern2.matcher(dto.getTitle());
        if (matcher2.find() && news.getCategory() != 3) {
            news.setCategory(3);
        }
        Date belongDate = DateTimeUtils.longParseDate(dto.getBelongDate());
        if (belongDate == null || belongDate.compareTo(DateTimeUtils.getBeforeDefault()) < 0) {
            return false;
        }
        news.setBelongDate(belongDate);
        return true;
    }

    public static List<String> segment(String text) {
        List<String> list = new ArrayList<>();
        try {
            StringReader re = new StringReader(text);
            IKSegmenter ik = new IKSegmenter(re, true);
            Lexeme lex;
            while ((lex = ik.next()) != null) {
                list.add(lex.getLexemeText());
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return list;
    }

    public synchronized static NewsSimpleDTO analysis(NewsDTO news, List<NewsSimpleDTO> list, int category) {
        String title = news.getTitle();
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        List<String> splitArr = FilterHelper.segment(title);
        List<String> splitArrTmp = splitArr;
        if (CollectionUtils.isEmpty(splitArr)) {
            return null;
        }
        NewsSimpleDTO normalData = null;
        NewsSimpleDTO duplicateData = null;
        NewsSimpleDTO existData = null;
        for (NewsSimpleDTO data : list) {
            splitArr = splitArrTmp;
            double score = 0;
            String dataTitle = data.getTitle();
            if (dataTitle.equalsIgnoreCase(title) && news.getAnnouncer().equalsIgnoreCase(data.getAnnouncer())) {
                existData = data;
                existData.setRate(1);
                break;
            }
            if (dataTitle.length() >= title.length()) {
                for (String split : splitArr) {
                    if (dataTitle.contains(split)) {
                        score++;
                    }
                }
            } else {
                splitArr = FilterHelper.segment(dataTitle);
                if (CollectionUtils.isEmpty(splitArr)) {
                    break;
                }
                for (String split : splitArr) {
                    if (title.contains(split)) {
                        score++;
                    }
                }
            }
            double rate = score / splitArr.size();
            double rateLimit = 0.5;
            if (category == 2) {
                rateLimit = 0.7;
            }
            if (rate > rateLimit) {
                System.out.println(JsonUtil.toString(data));
                if (data.getPid() != null && data.getPid() > 0) {
                    duplicateData = data;
                    duplicateData.setRate(rate);
                } else {
                    normalData = data;
                    normalData.setRate(rate);
                }
            }
        }
        if (existData != null) {
            return existData;
        }
        if (normalData != null) {
            return normalData;
        }
        return duplicateData;
    }

    public static void main(String[] args) {
//        System.out.println(publishDate("2018-12-06"));
        System.out.println(analysis(new NewsDTO("元戎启行自动驾驶解决方案：能感知到周围140米范围内的物体", "111"), Arrays.asList(new NewsSimpleDTO("声音 | 全国工商联邱小平：5G、区块链等新技术让并购有能力为企业和社会创造更大的价值", "222")), 0));
    }
}
