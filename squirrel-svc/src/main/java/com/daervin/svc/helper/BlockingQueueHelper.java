package com.daervin.svc.helper;

import com.daervin.svc.common.dto.NewsDTO;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author daervin
 * @date 2019/12/10
 */
public final class BlockingQueueHelper {
    private static LinkedBlockingQueue<List<NewsDTO>> linkedBlockingQueue;

    private BlockingQueueHelper() {
    }

    public static synchronized LinkedBlockingQueue<List<NewsDTO>> getInstance() {
        if (linkedBlockingQueue == null) {
            linkedBlockingQueue = new LinkedBlockingQueue<>();
        }
        return linkedBlockingQueue;
    }

    public static void put(List<NewsDTO> elemnet) {
        try {
            getInstance().put(elemnet);
        } catch (Exception e) {
            System.err.println("BlockingQueueHelper_PUT" + e.getMessage());
        }
    }

    public static List<NewsDTO> take() {
        try {
            return getInstance().take();
        } catch (Exception e) {
            System.err.println("BlockingQueueHelper_TAKE" + e.getMessage());
        }
        return null;
    }
}
