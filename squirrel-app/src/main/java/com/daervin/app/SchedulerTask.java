package com.daervin.app;

import com.daervin.svc.HotSpiderEngineProcess;
import com.daervin.svc.SpiderEngineProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时执行一些操作
 * <p>
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
public class SchedulerTask {
    @Autowired
    private SpiderEngineProcess spiderEngineProcess;
    @Autowired
    private HotSpiderEngineProcess hotSpiderEngineProcess;

    @Scheduled(cron = "0 0 0/1 * * ?")
    private void doNewsTask() {
        spiderEngineProcess.submitNewsJob();
    }

    @Scheduled(cron = "0 40 0/1 * * ? ")
    private void doHotTask() {
        hotSpiderEngineProcess.submit();
    }
}
