package com.daervin.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
@SpringBootApplication
@ComponentScan("com.daervin.*")
@MapperScan("com.daervin.svc.dal")
@EnableScheduling
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
