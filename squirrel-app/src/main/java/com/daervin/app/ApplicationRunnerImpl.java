package com.daervin.app;

import com.daervin.svc.HotSpiderEngineProcess;
import com.daervin.svc.SpiderEngineProcess;
import com.daervin.svc.service.IHotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 服务启动后做一些必要操作
 * <p>
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {
    @Autowired
    private SpiderEngineProcess spiderEngineProcess;
    @Autowired
    private HotSpiderEngineProcess hotSpiderEngineProcess;
    @Autowired
    private IHotService hotService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        spiderEngineProcess.takeThread();
        spiderEngineProcess.submitNewsJob();
        hotService.initHot();
        hotSpiderEngineProcess.submit();
    }
}
