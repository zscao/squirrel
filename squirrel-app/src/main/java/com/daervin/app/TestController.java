package com.daervin.app;

import com.daervin.svc.SpiderEngineProcess;
import com.daervin.svc.common.constants.ImageType;
import com.daervin.svc.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author daervin
 * @version 1.0.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private ImageService imageService;
    @Autowired
    private SpiderEngineProcess spiderEngineProcess;

    @RequestMapping("/get")
    public List<String> get() {
        return imageService.queryByType(ImageType.NORMAL);
    }

    @RequestMapping("spider")
    public String spider() {
        spiderEngineProcess.submitNewsJob();
        spiderEngineProcess.takeThread();
        return "true";
    }
}
