/*
Navicat MySQL Data Transfer

Source Server         : a_localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : squirrel

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-12-02 13:30:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dv_image`
-- ----------------------------
DROP TABLE IF EXISTS `dv_image`;
CREATE TABLE `dv_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(8000) COLLATE utf8mb4_bin DEFAULT '',
  `sort` int(11) DEFAULT '0',
  `type` int(11) DEFAULT '0' COMMENT '0 默认，1 pixabay',
  `from_url` varchar(8000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_type` (`sort`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of dv_image
-- ----------------------------

-- ----------------------------
-- Table structure for `dv_jieqi`
-- ----------------------------
DROP TABLE IF EXISTS `dv_jieqi`;
CREATE TABLE `dv_jieqi` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mpic` varchar(255) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of dv_jieqi
-- ----------------------------

-- ----------------------------
-- Table structure for `dv_news`
-- ----------------------------
DROP TABLE IF EXISTS `dv_news`;
CREATE TABLE `dv_news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `belongDate` datetime NOT NULL,
  `announcer` varchar(255) DEFAULT '',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT '初步分类（0 其它新闻、1科技新闻、2、融资收购）',
  `links` varchar(2000) DEFAULT '',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_belongDate` (`belongDate`),
  KEY `idx_category` (`category`),
  KEY `idx_title` (`title`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of dv_news
-- ----------------------------

-- ----------------------------
-- Table structure for `dv_spider`
-- ----------------------------
DROP TABLE IF EXISTS `dv_spider`;
CREATE TABLE `dv_spider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of dv_spider
-- ----------------------------

DROP TABLE IF EXISTS `dv_hot`;
CREATE TABLE `dv_hot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hot_id` int(10) NOT NULL DEFAULT '0' COMMENT '热点ID',
  `site_id` int(10) NOT NULL DEFAULT '0' COMMENT '资源站点ID',
  `site_name` varchar(200) NOT NULL DEFAULT '' COMMENT '资源站点名称',
  `topic` varchar(200) NOT NULL DEFAULT '' COMMENT '资源站点话题分类',
  `total` int(10) NOT NULL DEFAULT '0' COMMENT '话题总数',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_hot_id` (`hot_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `dv_hot_item`;
CREATE TABLE `dv_hot_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hot_id` int(10) NOT NULL DEFAULT '0' COMMENT '热点ID',
  `title` varchar(1000) NOT NULL DEFAULT '' COMMENT '话题名称',
  `link` varchar(2000) NOT NULL DEFAULT '' COMMENT '话题原站链接',
  `score` bigint(10) NOT NULL DEFAULT '0' COMMENT '话题分',
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_hot_id` (`hot_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
